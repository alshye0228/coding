package edu.pnw.cs.software_design.component_diagram;
/**
 * The component interface in the UML component diagram. 
 * @author Min Hye Jun, Rui Zhang, Victor Guo
 * @version 1.0
 */
public class ComponInterface implements IElement{
	/**
	 * The interface name.
	 */
	protected String interfaceName;
	/**
	 * The interface type.
	 */
	private InterfaceType interfaceType;
	/**
	 * Create a new component interface with interface name and type.
	 * @param interfaceName the interface name
	 * @param interfaceType the interface type
	 */
	public ComponInterface(String interfaceName, InterfaceType interfaceType) {
		super();
		this.interfaceName = interfaceName;
		this.interfaceType = interfaceType;
	}
	/**
	 * Create a new component interface with interface name.
	 * @param interfaceName the interface name
	 */
	public ComponInterface(String interfaceName) {
		super();
		this.interfaceName = interfaceName;
		this.interfaceType = InterfaceType.REQUIRED;
	}
	/**
	 * Change the name of interface.
	 */
	@Override
	public void setName(String name) {
		interfaceName = name;
	}
	/**
	 * Get the interface name.
	 */
	@Override
	public String getName() {
		return interfaceName;
	}
	/**
	 * Get the interface type.
	 * @return the interface type
	 */
	public InterfaceType getInterfaceType() {
		return interfaceType;
	}
	/**
	 * Change the interface type.
	 * @param interfaceType interface type
	 */
	public void setInterfaceType(InterfaceType interfaceType) {
		this.interfaceType = interfaceType;
	}
}
