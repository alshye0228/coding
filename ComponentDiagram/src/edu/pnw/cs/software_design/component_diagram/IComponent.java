package edu.pnw.cs.software_design.component_diagram;

import java.util.List;
/**
 * Component interface. 
 * @author Min Hye Jun, Rui Zhang, Victor Guo
 * @version 1.0
 */
public interface IComponent {
	/**
	 * add interface into component
	 * @param componInterface component interface
	 */	
	public void addInterface(ComponInterface componInterface);
	/**
	 * Get the interface list of the component
	 * @return interface list
	 */
	public List<ComponInterface> getInterface();
	/**
	 * Delete a interface in the component
	 * @param componInterface component interface
	 * @return true if delete operation is valid, or false
	 */
	public boolean deleteInterface(ComponInterface componInterface);
	/**
	 * add port into component
	 * @param port component port
	 */
	public void addPort(Port port);
	/**
	 * get the port list of the component
	 * @return port list
	 */
	public List<Port> getPort();
	/**
	 * Delete a port in the component
	 * @param port component port
	 * @return true if delete operation is valid, or false
	 */
	public boolean deletePort(Port port);
	/**
	 * Add inner component to a component.
	 * @param component inner component
	 */
	public void addInnerComponent(IComponent component);
	/**
	 * Get inner component list.
	 * @return inner component list
	 */
	public List<IComponent> getInnerComponent();
	/**
	 * Get the component name
	 * @return component name string
	 */
	public String getName();
	/**
	 * Get the component interface by interface name.
	 * @param interfaceName interface name
	 * @return component interface
	 */
	public ComponInterface getInterface(String interfaceName);
	/**
	 * Get the port by port name. 
	 * @param portName port name
	 * @return component port
	 */
	public Port getPort(String portName);
}