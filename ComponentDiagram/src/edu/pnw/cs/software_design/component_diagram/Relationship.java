package edu.pnw.cs.software_design.component_diagram;
/**
 * Relationship in the UML component diagram. The relationship includes start part and end part.
 * The start part must have a start component and may have a start port or start interface.
 * The end part must have a end component and may have a end port or end interface.
 * @author Min Hye Jun, Rui Zhang, Victor Guo
 * @version 1.0
 */
public class Relationship implements IRelationship{
	/**
	 * The relationship relation type.
	 */
	private RelationType relationType;
	/**
	 * The relationship start component.
	 */
	private Component startComponent;
	/**
	 * The relationship end component.
	 */
	private Component endComponent;
	/**
	 * The relationship start interface. 
	 */
	private ComponInterface startInterface;
	/**
	 * The relationship end interface.
	 */
	private ComponInterface endInterface;
	/**
	 * The relationship start port.
	 */
	private Port startPort;
	/**
	 * The relationship end port.
	 */
	private Port endPort;
	/**
	 * Create a new Relationship.
	 * @param relationType the relationship type
	 * @param startComponent the start component of the relationship
	 * @param endComponent the end component of the relationship
	 */
	public Relationship(RelationType relationType, Component startComponent, Component endComponent) {
		super();
		this.relationType = relationType;
		this.startComponent = startComponent;
		this.endComponent = endComponent;
	}

	public Relationship(Component startComponent, Component endComponent, ComponInterface startInterface, ComponInterface endInterface, 
			Port startPort, Port endPort, RelationType relationType) {
		super();
		this.startComponent = startComponent;
		this.endComponent = endComponent;
		this.startInterface = startInterface;
		this.endInterface = endInterface;
		this.startPort = startPort;
		this.endPort = endPort;
		this.relationType = relationType;

	}

	@Override
	public void setRelationshipType(RelationType relationType) {
		this.relationType = relationType;	
	}

	@Override
	public RelationType getRelationshipType() {
		return relationType;
	}

	@Override
	public void setStartComponent(Component component) {
		this.startComponent = component;
	}

	@Override
	public Component getStartComponent() {
		return startComponent;
	}

	@Override
	public void setEndComponent(Component component) {
		this.endComponent = component;
	}

	@Override
	public Component getEndComponent() {
		return endComponent;
	}

	@Override
	public void setStartInterface(ComponInterface newInterface) {
		this.startInterface = newInterface;
	}

	@Override
	public ComponInterface getStartInterface() {
		return startInterface;
	}

	@Override
	public void setEndInterface(ComponInterface newInterface) {
		this.endInterface = newInterface;
	}

	@Override
	public ComponInterface getEndInterface() {
		return endInterface;
	}

	@Override
	public void setStartPort(Port port) {
		this.startPort = port;
	}

	@Override
	public Port getStartPort() {
		return startPort;
	}

	@Override
	public void setEndPort(Port port) {
		this.endPort = port;
	}

	@Override
	public Port getEndPort() {
		return endPort;
	}

	public int hashCode(){
		return 2;	
	}

	public boolean equals(Relationship r) {
		if(startComponent.equals(r.getStartComponent())) 
			if(endComponent.equals(r.getEndComponent())) { 
				if((startInterface == null && r.getStartInterface() == null) 
						|| (startInterface != null 
						&& r.getStartInterface() != null 
						&& startInterface.getName().equals(r.getStartInterface().getName())))
					if((endInterface == null && r.getEndInterface() == null) 
							|| (endInterface != null && r.getEndInterface() != null 
							&& endInterface.getName().equals(r.getEndInterface().getName())))
						if((startPort == null && r.getStartPort() == null) 
								|| (startPort != null && r.getStartPort() != null 
								&& startPort.getName().equals(r.getStartPort().getName())))
							if((endPort == null && r.getEndPort() == null) 
									|| (endPort != null && r.getEndPort() != null 
									&& endPort.getName().equals(r.getEndPort().getName()))) 
								if(relationType.equals(r.getRelationshipType())) {
									return true;
								}
			}
		return false;
	}

}