package edu.pnw.cs.software_design.component_diagram;
/**
 * Iterface type: REQUIRED, PROVIDED. 
 * @author Min Hye Jun, Rui Zhang, Victor Guo
 * @version 1.0
 */
public enum InterfaceType {
	REQUIRED, PROVIDED;
}