package edu.pnw.cs.software_design.component_diagram;

import java.util.*;
/**
 * The UML component diagram class, all adding, removing, searching, modifying, creating and validating 
 * methods are included in this class.
 * @author Min Hye Jun, Rui Zhang, Victor Guo
 * @version 1.0
 */
public class UMLComponentDiagram {

	private HashMap<ComponInterface, Component> interfaceComponentHashMap = new HashMap<ComponInterface, Component>();
	private HashMap<Port, Component> portComponentHashMap = new HashMap<Port, Component>();

	private HashMap<Component, ArrayList<IRelationship>> componentRelationHashMap = new HashMap<Component, ArrayList<IRelationship>>();
	private ArrayList<IComponent> componentList = new ArrayList<IComponent>();
	private ArrayList<IRelationship> relationshipList = new ArrayList<IRelationship>();

	/**
	 * Add interface into specific component.
	 * @param newInterface new interface name
	 * @param componentName component name
	 * @return true if the interface is added successfully, or false
	 */
	public boolean addInterface(ComponInterface newInterface, String componentName){
		//get component by component name.
		Component component = this.getComponent(componentName);
		if(component != null && !validateInterface(newInterface,component)){
			// add interface into component 
			component.addInterface(newInterface);
			// add interface to HashMap<ComponInterface, Component>
			interfaceComponentHashMap.put(newInterface, component);

			return true;

		}
		return false;
	}

	/**
	 * Add port into specific component.
	 * @param port port name
	 * @param componentName component name
	 * @return true if the port is added successfully, or false
	 */
	public boolean addPort(Port port, String componentName){
		Component component = this.getComponent(componentName);

		if(component != null && !validatePort(port,component)){
			// add interface into component
			component.addPort(port);
			// add interface to HashMap<Port, Component>
			portComponentHashMap.put(port, component);
			return true;
		}
		return false;
	}

	/**
	 * Add component into the component List.
	 * @param component new component
	 */
	public void addComponent(Component component){
		componentList.add(component);
	}

	/**
	 * Add component into parent component.
	 * @param parentComponent parent component
	 * @param component new component
	 */
	public void addChildComponent(Component parentComponent, Component component){
		parentComponent.addInnerComponent(component);
		addComponent(component);
	}

	/**
	 * Create a new component.
	 * @param componentName component name
	 * @return the new component if there is no duplicate component, or null
	 */
	public Component createCompoent(String componentName){
		if(getComponent(componentName) == null){
			Component component = new Component(componentName);
			componentList.add(component);
			return component;
		}
		return null;
	}
	/**
	 * Create a valid relationship into UML diagram.
	 * @param startComponent start component
	 * @param endComponent end component
	 * @param startInterface start interface
	 * @param endInterface end interface
	 * @param startPort start port
	 * @param endPort end port
	 * @param relationType relationship type
	 * @return true if the creation is completed, or false
	 */


	public boolean createRelationship(Component startComponent, Component endComponent, 
			ComponInterface startInterface, ComponInterface endInterface,
			Port startPort, Port endPort, RelationType relationType){

		Relationship relationship =  new Relationship(startComponent, endComponent, startInterface, endInterface, startPort, endPort, relationType);

		// check whether there is same relationship.
		if(validateRelationship(relationship)){
			return false;
		}

		// if startComponent or endComponent is not in UML diagram, return null.
		if(!this.validateComponent(startComponent) || !this.validateComponent(endComponent)){
			return false;
		}

		// if there is a start interface, it should be validate.
		if(startInterface != null){
			if(!validateInterface(startInterface,startComponent)){
				return false;
			}else{
				relationship.setStartInterface(startInterface);       
			}
		}
		// if there is a end interface, it should be validate.
		if(endInterface != null){
			if(!validateInterface(endInterface,endComponent)){
				return false;
			}else{
				relationship.setEndInterface(endInterface);       
			}
		}
		// if there is a start port, it should be validate.
		if(startPort != null){
			if(!validatePort(startPort,startComponent)){
				return false;
			}else{
				relationship.setStartPort(startPort);       
			}
		}
		// if there is a end port, it should be validate.
		if(endPort != null){
			if(!validatePort(endPort,endComponent)){
				return false;
			}else{
				relationship.setEndPort(endPort);       
			}
		}

		// the start interface can not be PROVIDED, the end interface can not be REQUIRED.
		if((startInterface!=null && startInterface.getInterfaceType().equals(InterfaceType.PROVIDED))
				|| (endInterface !=null && endInterface.getInterfaceType().equals(InterfaceType.REQUIRED))){
			return false;
		}

		if((endInterface == null && startInterface != null ) // if there is a start interface, there should be a end interface.
				||(startPort != null && startInterface == null)   // if there is a start port, there should be a start interface.
				||(endPort != null && endInterface == null)){  // if there is a end port, there should be a end interface.
			return false;
		}

		// add this relationship to relationshipList
		relationshipList.add(relationship);
		// Check whether the start component is one of the keys in the relationshipComponentHashMap,
		// if yes, add this relationship to the corresponding list,
		// if no, create a new ArrayList<IRelationship> , and this relationship will be included.
		if(componentRelationHashMap.containsKey(startComponent)){
			componentRelationHashMap.get(startComponent).add(relationship); 
		}else{
			ArrayList<IRelationship> relationshipList = new ArrayList<IRelationship>();
			relationshipList.add(relationship);
			componentRelationHashMap.put(startComponent, relationshipList);
		}
		// Check whether the end component is one of the keys in the relationshipComponentHashMap,
		// if yes, add this relationship to the corresponding list,
		// if no, create a new ArrayList<IRelationship> , and this relationship will be included.
		if(componentRelationHashMap.containsKey(endComponent)){
			componentRelationHashMap.get(endComponent).add(relationship); 
		}else{
			ArrayList<IRelationship> relationshipList = new ArrayList<IRelationship>();
			relationshipList.add(relationship);
			componentRelationHashMap.put(endComponent, relationshipList);
		}

		return true;

	}

	/**
	 * Get the list of components.
	 * @return component List in the diagram
	 */

	public ArrayList<IComponent> getListOfComponent(){
		return (ArrayList<IComponent>) componentList;
	}
	/**
	 * Get the interfaces in the specific component.
	 * @param component specific component
	 * @return component interface list
	 */
	public ArrayList<ComponInterface> getInterface(Component component) {
		return (ArrayList<ComponInterface>) component.getInterface();
	}

	/**
	 * Get relationship with specific component
	 * @param component component
	 * @return  relationship list
	 */
	public ArrayList<IRelationship> getRelationship(Component component) {
		ArrayList<IRelationship> relationship = componentRelationHashMap.get(component);
		return relationship;
	}

	/**
	 * Get a component by component name.
	 * @param componentName component name
	 * @return component
	 */
	public Component getComponent(String componentName) {

		for(IComponent component : componentList){
			component = (Component) component;
			if((component).getName().equals(componentName)){
				return (Component) component;
			}
		}
		return null;
	}
	/**
	 * Get all component ports in a component
	 * @param component component
	 * @return port list
	 */
	public ArrayList<Port> getPort(Component component) {
		return (ArrayList<Port>) component.getPort();
	}
	/**
	 * Get Relationship list by specific relationship type.
	 * @param relationType relationship type
	 * @return relationship list
	 */
	public ArrayList<IRelationship> searchRelationship(Component component1, Component component2){

		ArrayList<IRelationship> component1Relationship = getRelationship(component1);
		ArrayList<IRelationship> returnList = new ArrayList<IRelationship>();
		for(IRelationship relationship:component1Relationship){
			relationship = (Relationship) relationship;
			if(relationship.getStartComponent().equals(component2)
					||relationship.getEndComponent().equals(component2)){
				returnList.add(relationship);
			}
		}
		return returnList;
	}

	/**
	 * Get component interface list by a specific interface name, 
	 * the number of return interfaces could be 0, 1 or more.
	 * @param interfaceName component interface name
	 * @return component interface list
	 */
	public ArrayList<ComponInterface> searchInterface(String interfaceName){
		ArrayList<ComponInterface> interfaceList = new ArrayList<ComponInterface>();
		for (IElement key : interfaceComponentHashMap.keySet()) {
			if(key.getName().equals(interfaceName) && key instanceof ComponInterface){
				interfaceList.add((ComponInterface) key);
			}
		}
		return interfaceList;
	}

	/**
	 * Get component port list by a specific port name, 
	 * the number of return port could be 0, 1 or more.
	 * @param portName component port name
	 * @return component port list
	 */
	public ArrayList<Port> searchPort(String portName){ 
		ArrayList<Port> portList = new ArrayList<Port>();
		for (IElement key : portComponentHashMap.keySet()) {
			if(key.getName().equals(portName) && key instanceof Port){
				portList.add((Port) key);
			}
		}
		return portList;  
	}
	/**
	 * Delete component by the specific component name. The related interfaces, ports 
	 * and inner component will be removed from corresponding data structure.
	 * @param componentName component name
	 * @return true if the component is deleted successfully, or false
	 */
	public boolean deleteComponentByName(String componentName){
		Iterator<IComponent> i = componentList.iterator();

		while(i.hasNext()) {
			Component component = (Component) i.next();
			if(componentName.equals(component.getName())) {
				// remove the parent component from the component list.
				componentList.remove(component);               
				componentRelationHashMap.remove(component);
				// remove the interfaces from the interfaceComponentHashMap.
				Iterator<ComponInterface> interface_i = component.getInterface().iterator();
				while(interface_i.hasNext()) {
					interfaceComponentHashMap.remove(interface_i.next());
				}
				// remove the port from the portComponentHashMap.
				Iterator<Port> port_i = component.getPort().iterator();
				while(port_i.hasNext()) {
					portComponentHashMap.remove(port_i.next());
				}
				// remove inner component from the component list
				List<IComponent> innerComponentList = component.getInnerComponent();
				for(IComponent componentIterator : innerComponentList){
					componentList.remove(componentIterator); 
				}
				return true;

			}
		}
		return false;
	}
	/**
	 * Delete component interface in the specific component name. 
	 * @param interfaceName interface name
	 * @param componentName component name
	 * @return true if the interface is deleted successfully, or false 
	 */
	public boolean deleteInterfaceByName(String interfaceName, String componentName){
		Component component = this.getComponent(componentName);
		if(component!=null){
			ComponInterface specificInterface = component.getInterface(interfaceName);
			if(specificInterface!=null){
				interfaceComponentHashMap.remove(specificInterface);
				component.deleteInterface(specificInterface);
				return true;
			}
		}
		return false;
	}
	/**
	 * Delete component port in the specific component name. 
	 * @param portName port name
	 * @param componentName component name
	 * @return true if the port is deleted successfully, or false 
	 */
	public boolean deletePortByName(String portName, String componentName){
		Component component = this.getComponent(componentName);
		if(component!=null){
			Port port = component.getPort(portName);
			if(port != null) {
				portComponentHashMap.remove(port);
				component.deletePort(port);
				return true;
			}
		}
		return false;
	}
	/**
	 * Delete a specific relationship.
	 * @param relationship relationship
	 * @return true if the relationship is deleted successfully, or false 
	 */
	public boolean deleteRelationship(Relationship relationship) {

		boolean result = false;
		
		Iterator<IRelationship> it = relationshipList.iterator();
		while(it.hasNext()) {
			Relationship r = (Relationship)it.next();
			if(r.equals(relationship)) {
				relationshipList.remove(r);
				break;
			}
		}

		Iterator<Component> i = componentRelationHashMap.keySet().iterator();
		while(i.hasNext()) {
			Component com = (Component) i.next();
			ArrayList <IRelationship> relationList = componentRelationHashMap.get(com);
			Iterator<IRelationship> i2 = relationList.iterator();
			while(i2.hasNext()) {
				Relationship r = (Relationship) i2.next();
				if(r.equals(relationship)) {
					componentRelationHashMap.get(com).remove(r);
					result = true;
					break;
				}
			}
		} 
		return result;
	}
	/**
	 * Validate the relationship.
	 * @param relationship input relationship
	 * @return true if the relationship exist, or false 
	 */

	public boolean validateRelationship(Relationship relationship) {
		for(IRelationship r: relationshipList) {
			if(r.equals(relationship)){
				return true;
			}
		}
		return false;
	}

	/**
	 * Validate the component interface by specific component and interface. 
	 * or false will be returned.
	 * @param component component
	 * @param componInterface component interface
	 * @return true if the relationship exist, or false
	 */
	public boolean validateInterface(ComponInterface componInterface, Component component){
		if(interfaceComponentHashMap.containsKey(componInterface)){
			if(interfaceComponentHashMap.get(componInterface).equals(component)){
				return true;
			}
		}
		return false; 
	}
	/**
	 * Validate the component port by specific component and port. 
	 * @param component component
	 * @param port component port
	 * @return true if the port exist, or false
	 */
	public boolean validatePort(Port port, Component component){
		if(portComponentHashMap.containsKey(port)){
			if(portComponentHashMap.get(port).equals(component)){
				return true;
			}
		}
		return false; 
	}
	/**
	 *Validate the component by specific component name. 
	 * @param componentName component name.
	 * @return true if the port exist, or false
	 */
	public boolean validateComponent(String componentName){
		if(getComponent(componentName) != null){
			return true;
		}
		return false;
	}

	public boolean validateComponent(Component component){   
		if(getComponent(component.getName()) != null){
			return true;
		}
		return false;
	}
	/**
	 * Modify the component name.
	 * @param originalName original component name
	 * @param newName new component name
	 * @return revised component
	 */
	public Component modifyComponentName(String originalName, String newName){
		Component component = this.getComponent(originalName);
		component.setName(newName);
		return component; 
	}
	/**
	 * Modify the component interface name.
	 * @param originalName original interface name
	 * @param newName new interface name
	 * @param componentName component name
	 * @return revised component interface
	 */
	public ComponInterface modifyInterfaceName(String originalName, String newName, String componentName){
		Component component = this.getComponent(componentName);
		ComponInterface componInterface = component.getInterface(originalName);
		if(componInterface != null){
			componInterface.setName(newName);
			return componInterface;
		}
		return null;
	}
	/**
	 * Modify the component port name.
	 * @param originalName original port name
	 * @param newName new port name
	 * @param componentName component name
	 * @return revised port
	 */
	public Port modifyPortName(String originalName,String newName, String componentName){
		Component component = this.getComponent(componentName);
		Port port = component.getPort(originalName);
		if(port != null){
			port.setName(newName);
			return port;
		}
		return null;
	}
	/**
	 * Get the owner component of a specific interface.
	 * @param componInterface component interface
	 * @return owner component
	 */
	public Component ownerOfInterface(ComponInterface componInterface){
		Iterator<ComponInterface> i = interfaceComponentHashMap.keySet().iterator();

		while(i.hasNext()) {
			ComponInterface in = (ComponInterface) i.next();
			if((in.getName()).equals(componInterface.getName())) 
				return interfaceComponentHashMap.get(in);
		}
		return null;
	}
	/**
	 * Get the owner component of a specific port.
	 * @param port component port
	 * @return owner component
	 */
	public Component ownerOfPort(Port port){
		Iterator<Port> i = portComponentHashMap.keySet().iterator();

		while(i.hasNext()) {
			Port p = (Port) i.next();
			if((p.getName()).equals(port.getName())) 
				return portComponentHashMap.get(p);
		}

		return null;
	}
}