package edu.pnw.cs.software_design.component_diagram;
/**
 * Port in the UML component diagram.
 * @author Min Hye Jun, Rui Zhang, Victor Guo
 * @version 1.0
 */
public class Port implements IElement{
	/**
	 * Port name.
	 */
	protected String portName;
	/**
	 * Create a new port with port name.
	 * @param portName port name
	 */
	public Port(String portName) {
		super();
		this.portName = portName;
	}
	/**
	 * Set the port name.
	 * @param name port name
	 */
	@Override
	public void setName(String name) {
		portName = name;
	}
	/**
	 * Get the port name.
	 * @return port name
	 */
	@Override
	public String getName() {
		return portName;
	}
	
	
}
