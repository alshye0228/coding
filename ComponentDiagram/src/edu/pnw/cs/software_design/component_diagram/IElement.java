package edu.pnw.cs.software_design.component_diagram;
/**
 * Element interface, the element includes the component port and component interface. 
 * @author Min Hye Jun, Rui Zhang, Victor Guo
 * @version 1.0
 */
public interface IElement {
	/**
	 * Change the element name
	 * @param name element name
	 */
	public void setName(String name);
	/**
	 * Get the element name.
	 * @return element name string
	 */
	public String getName();
}