package edu.pnw.cs.software_design.component_diagram;

import java.util.ArrayList;
import java.util.List;

/**
 * The component in the UML component diagram. The component includes a list of interfaces 
 * and a list of ports, and it may has a list of inner component.
 * @author Min Hye Jun, Rui Zhang, Victor Guo
 * @version 1.0
 */
public class Component implements IComponent, IElement{
	/**
	 * The component name.
	 */
	protected String componentName;
	/**
	 * The list of interface in the component.
	 */
	private List<ComponInterface> interfaceList = new ArrayList<ComponInterface>();
	/**
	 * The list of inner component in the component.
	 */
	private List<IComponent> innerComponentList = new ArrayList<IComponent>();
	/**
	 * The list of port in the component.
	 */
	private List<Port> portList = new ArrayList<Port>();

	/**
	 * Constructs a Component using componentName 
	 * @param componentName new component's name
	 */
	public Component(String componentName) {
		super();
		this.componentName = componentName;
	}

	@Override
	public void setName(String componentName) {
		this.componentName = componentName;
	}

	@Override
	public String getName() {
		return componentName;
	}

	@Override
	public void addInterface(ComponInterface i) {
		interfaceList.add(i);
	}

	@Override
	public boolean deleteInterface(ComponInterface i) {
		if(interfaceList.remove(i))
			return true;
		else 
			return false;
	}

	@Override
	public List<ComponInterface> getInterface() {
		return interfaceList;
	}

	@Override
	public void addPort(Port port) {
		portList.add(port);
	}

	@Override
	public List<Port> getPort() {
		return portList;
	}

	@Override
	public void addInnerComponent(IComponent component) {
		innerComponentList.add(component);
	}

	@Override
	public List<IComponent> getInnerComponent() {
		return innerComponentList;
	}

	@Override
	public boolean deletePort(Port port) {
		if(portList.remove(port))
			return true;
		else
			return false;
	}

	/**
	 * Set the hashCode of Component object.
	 */
	public int hashCode(){
		return 1;	
	}
	/**
	 * Overwrite the equals method
	 * @return true if equal, or false
	 */
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Component other = (Component) obj;
		if (componentName != other.getName())
			return false;
		return true;

	}
	@Override
	public ComponInterface getInterface(String originalName) {
		for(ComponInterface componInterafce : interfaceList){
			if(componInterafce.getName().equals(originalName)){
				return componInterafce;
			}
		}
		return null;
	}
	@Override
	public Port getPort(String originalName) {
		for(Port iteratePort : portList){
			if(iteratePort.getName().equals(originalName)){
				return iteratePort;
			}
		}
		return null;
	}
}
