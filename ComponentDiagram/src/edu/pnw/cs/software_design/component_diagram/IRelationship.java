package edu.pnw.cs.software_design.component_diagram;
/**
 * IRelationship interface. 
 * @author Min Hye Jun, Rui Zhang, Victor Guo
 * @version 1.0
 */
public interface IRelationship {
	/**
	 * Set the relationship type.
	 * @param relationshipType the relationship type
	 */
	public void setRelationshipType(RelationType relationshipType);
	/**
	 * Get the relationship type.
	 * @return the relationship type
	 */
	public RelationType getRelationshipType();
	
	
	// set and get component.
	/**
	 * Set the start component of relationship.
	 * @param component the start component
	 */
	public void setStartComponent(Component component);
	/**
	 * Get the start component of relationship.
	 * @return the start component
	 */
	public Component getStartComponent();
	/**
	 * Set the end component of relationship.
	 * @param component the end component
	 */
	public void setEndComponent(Component component);
	/**
	 * Get the end component of relationship.
	 * @return the end component
	 */
	public Component getEndComponent();
	
	// set and get interface.
	/**
	 * Set the start interface of relationship.
	 * @param newInterface the start interface
	 */
	public void setStartInterface(ComponInterface newInterface);
	/**
	 * Get the start interface of relationship.
	 * @return the start interface
	 */
	public ComponInterface getStartInterface();
	/**
	 * Set the end interface of relationship.
	 * @param newInterface the end interface
	 */
	public void setEndInterface(ComponInterface newInterface);
	/**
	 * Get the end interface of relationship.
	 * @return the end interface
	 */
	public ComponInterface getEndInterface();
	// set and get port.
	/**
	 * Set the start port of relationship.
	 * @param port the start port
	 */
	public void setStartPort(Port port);
	/**
	 * Get the start port of relationship.
	 * @return the start port
	 */
	public Port getStartPort();
	/**
	 * Set the end port of relationship
	 * @param port the end port
	 */
	public void setEndPort(Port port);
	/**
	 * Get the end port of relationship.
	 * @return the end port
	 */
	public Port getEndPort();
	/**
	 * Compare with input relationship.
	 * @param relationship input relationship
	 * @return true if the input relationship is the same as this relationship, or false
	 */
	public boolean equals(Relationship relationship);
}