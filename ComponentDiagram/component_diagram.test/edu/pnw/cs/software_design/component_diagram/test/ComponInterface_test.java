package edu.pnw.cs.software_design.component_diagram.test;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.pnw.cs.software_design.component_diagram.ComponInterface;
import edu.pnw.cs.software_design.component_diagram.InterfaceType;

public class ComponInterface_test {

 @Test
 public void test() {
  ComponInterface in1 = new ComponInterface("interface1", InterfaceType.PROVIDED);
  ComponInterface in2 = new ComponInterface("interface2");
  assertEquals(in1.getInterfaceType(), InterfaceType.PROVIDED);
  assertEquals(in2.getInterfaceType(), InterfaceType.REQUIRED);
  in1.setName("interface_1");
  in2.setName("interface_2");
  assertEquals(in1.getName(), "interface_1");
  assertEquals(in2.getName(), "interface_2");
 }
}