package edu.pnw.cs.software_design.component_diagram.test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import edu.pnw.cs.software_design.component_diagram.Component;
import edu.pnw.cs.software_design.component_diagram.IComponent;
import edu.pnw.cs.software_design.component_diagram.ComponInterface;
import edu.pnw.cs.software_design.component_diagram.InterfaceType;
import edu.pnw.cs.software_design.component_diagram.Port;

public class Component_test {

 @Test
 public void componentName_test() {
  Component c = new Component("Component 1");
  assertEquals(c.getName(), "Component 1");
  c.setName("Component 2");
  assertEquals(c.getName(), "Component 2");
 }
 
 @Test
 public void interface_test() {
  Component c = new Component("Component 1");
  ComponInterface i = new ComponInterface("interface 1", InterfaceType.PROVIDED);
  c.addInterface(i);
  List<ComponInterface> iList = new ArrayList <ComponInterface>();
  iList.add(i);
  assertEquals(c.getInterface(), iList);
  assertEquals(c.deleteInterface(i), true);
 }
 
 @Test
 public void port_test() {
  Component c = new Component("Component 1");
  Port p = new Port("port 1");
  c.addPort(p);
  List<Port> pList = new ArrayList <Port>();
  pList.add(p);
  assertEquals(c.getPort(), pList);
  assertEquals(c.deletePort(p), true);
 }
 
 @Test
 public void innerComponent_test() {
  Component c = new Component("Component 1");
  Component inner_c = new Component("Component 3");
  c.addInnerComponent(inner_c);
  List<IComponent> innerComponentList = new ArrayList<IComponent>();
  innerComponentList.add(inner_c);
  assertEquals(c.getInnerComponent(), innerComponentList);
 }
}