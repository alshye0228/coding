package edu.pnw.cs.software_design.component_diagram.test;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.pnw.cs.software_design.component_diagram.ComponInterface;
import edu.pnw.cs.software_design.component_diagram.Component;
import edu.pnw.cs.software_design.component_diagram.Port;
import edu.pnw.cs.software_design.component_diagram.RelationType;
import edu.pnw.cs.software_design.component_diagram.Relationship;

public class Relationship_test {

 @Test
 public void test() {
  Component com1 = new Component("StartComponent");
  Component com2 = new Component("EndComponent");
  ComponInterface in1 = new ComponInterface("StartInterface");
  ComponInterface in2 = new ComponInterface("EendInterface");
  Port p1 = new Port("StartPort");
  Port p2 = new Port("EndPort");
  
  Relationship r = new Relationship(RelationType.LINK, com1, com2);
  assertEquals(r.getStartComponent(), com1);
  assertEquals(r.getEndComponent(), com2);
  assertEquals(r.getRelationshipType(), RelationType.LINK);
  
  Component com3 = new Component("newStartComponent");
  Component com4 = new Component("newEndComponent");
  r.setStartComponent(com3);
  r.setEndComponent(com4);
  assertEquals(r.getStartComponent(), com3);
  assertEquals(r.getEndComponent(), com4);
  
  r.setStartInterface(in1);
  r.setEndInterface(in2);
  assertEquals(r.getStartInterface(), in1);
  assertEquals(r.getEndInterface(), in2);
  
  r.setStartPort(p1);
  r.setEndPort(p2);
  assertEquals(r.getStartPort(), p1);
  assertEquals(r.getEndPort(), p2);
  
  
  
 }
}
