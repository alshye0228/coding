package edu.pnw.cs.software_design.component_diagram.test;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.pnw.cs.software_design.component_diagram.Port;

public class Port_test {

 @Test
 public void test() {
  Port port = new Port("port");
  assertEquals(port.getName(), "port");
  port.setName("port*");
  assertEquals(port.getName(), "port*");
 }

}
