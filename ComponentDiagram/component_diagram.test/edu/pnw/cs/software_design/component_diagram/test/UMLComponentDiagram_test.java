package edu.pnw.cs.software_design.component_diagram.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import org.junit.Before;
import org.junit.Test;

import edu.pnw.cs.software_design.component_diagram.ComponInterface;
import edu.pnw.cs.software_design.component_diagram.Component;
import edu.pnw.cs.software_design.component_diagram.IComponent;
import edu.pnw.cs.software_design.component_diagram.IRelationship;
import edu.pnw.cs.software_design.component_diagram.InterfaceType;
import edu.pnw.cs.software_design.component_diagram.Port;
import edu.pnw.cs.software_design.component_diagram.RelationType;
import edu.pnw.cs.software_design.component_diagram.Relationship;
import edu.pnw.cs.software_design.component_diagram.UMLComponentDiagram;

public class UMLComponentDiagram_test {
	UMLComponentDiagram umlDiagram = new UMLComponentDiagram();
	Component componentA = new Component("ComponentA");
	Component componentB = new Component("ComponentB");
	ComponInterface requiredInterface = new ComponInterface("RequiredInterface", InterfaceType.REQUIRED);
	ComponInterface providedInterface = new ComponInterface("ProvidedInterface", InterfaceType.PROVIDED);
	Port portA = new Port("PortA");
	Port portB = new Port("PortB");
	
	@Before
	public void init(){
		umlDiagram = new UMLComponentDiagram();
		componentA = new Component("ComponentA");
		componentB = new Component("ComponentB");
		requiredInterface = new ComponInterface("RequiredInterface", InterfaceType.REQUIRED);
		providedInterface = new ComponInterface("ProvidedInterface", InterfaceType.PROVIDED);
		portA = new Port("PortA");
		portB = new Port("PortB");
		
		umlDiagram.addComponent(componentA);
	}
	
	@Test
	public void CreateComponentTest(){
		Component componentC = new Component("ComponentC");
		umlDiagram.createCompoent("ComponentC");
		Component resultComponent = umlDiagram.getComponent("ComponentC");
		assertEquals(resultComponent,componentC);
	}
	@Test
	public void AddComponentTest() {
		assertEquals(umlDiagram.getComponent("ComponentA").getName(),"ComponentA");
	}
	@Test
	public void AddChildComponentTest(){
		assertEquals(umlDiagram.getComponent("ComponentA").getName(),"ComponentA");
		
		Component innerComponentA1 = new Component("innerComponentA1");
		Component innerComponentA2 = new Component("innerComponentA2");

		umlDiagram.addChildComponent(componentA, innerComponentA1);
		umlDiagram.addChildComponent(componentA, innerComponentA2);

		ArrayList<IComponent> innerComponentList = (ArrayList<IComponent>) this.componentA.getInnerComponent();
		ArrayList<IComponent> compareComponentList = new ArrayList<IComponent>();
		compareComponentList.add(innerComponentA1);
		compareComponentList.add(innerComponentA2);

		assertEquals(innerComponentList,compareComponentList);

	}
	@Test
	public void GetComponentTest() {

		Component component = umlDiagram.getComponent("ComponentA");
		assertEquals(component,componentA);
	}

	@Test 
	public void getListOfComponentTest() {
		umlDiagram.addComponent(componentB);
		ArrayList<IComponent> componentList = new ArrayList<IComponent>();
		componentList.add(componentA);
		componentList.add(componentB);
		assertEquals(umlDiagram.getListOfComponent(), componentList);
	}

	@Test
	public void AddInterfaceTest(){
		ComponInterface interface1 = new ComponInterface("Interface1", InterfaceType.REQUIRED);

		boolean result = umlDiagram.addInterface(interface1, "ComponentA");
		assertTrue(result);

		assertEquals(this.componentA.getInterface("Interface1"),interface1);

		// add the same interface.
		boolean result2 = umlDiagram.addInterface(interface1, "ComponentA");
		assertFalse(result2);

	}

	@Test
	public void GetInterfaceTest(){
		ComponInterface interface1 = new ComponInterface("Interface1", InterfaceType.REQUIRED);
		ComponInterface interface2 = new ComponInterface("Interface2", InterfaceType.PROVIDED);

		umlDiagram.addInterface(interface1, "ComponentA");
		umlDiagram.addInterface(interface2, "ComponentA");

		ArrayList<ComponInterface> componInterfaceList = umlDiagram.getInterface(componentA);

		ArrayList<ComponInterface> compareComponInterfaceList = new ArrayList<ComponInterface>();
		compareComponInterfaceList.add(interface1);
		compareComponInterfaceList.add(interface2);

		assertEquals(componInterfaceList,compareComponInterfaceList);

	}

	@Test
	public void AddPortTest(){

		Port port1 = new Port("Port1");

		boolean result = umlDiagram.addPort(port1, "ComponentA");
		assertTrue(result);
		result = umlDiagram.addPort(port1, "ComponentA");
		assertFalse(result);


	}

	@Test
	public void GetPortTest(){
		Port port1 = new Port("Port1");
		Port port2 = new Port("Port2");


		umlDiagram.addPort(port1, "ComponentA");
		umlDiagram.addPort(port2, "ComponentA");

		ArrayList<Port> portList = umlDiagram.getPort(componentA);
		ArrayList<Port> comparePortList = new ArrayList<Port>();

		comparePortList.add(port1);
		comparePortList.add(port2);

		assertEquals(portList,comparePortList);
	}

	@Test
	public void CreateRelationshipTest1(){

		umlDiagram.addComponent(componentB);
		// create relationship only with two component.
		boolean result = umlDiagram.createRelationship(componentA, componentB,null,null,null,null, RelationType.AGGREGATION);
		assertTrue(result);
		result = umlDiagram.createRelationship(componentA, componentB,null,null,null,null, RelationType.AGGREGATION);
		assertFalse(result);

		ArrayList<IRelationship> resultList = umlDiagram.searchRelationship(componentA, componentB);

		Relationship relationship= (Relationship) resultList.get(0);
		assertEquals(relationship.getStartComponent(),componentA);
		assertEquals(relationship.getEndComponent(),componentB);

		result = umlDiagram.addInterface(requiredInterface,"ComponentA");
		assertTrue(result);
		result = umlDiagram.addInterface(providedInterface,"ComponentB");
		assertTrue(result);
		result = umlDiagram.validateInterface(requiredInterface,componentA);
		assertTrue(result);

		// create relationship with two component, two interface.
		result = umlDiagram.createRelationship(componentA, componentB,requiredInterface,providedInterface,null,null, RelationType.AGGREGATION);
		assertTrue(result);
		result = umlDiagram.createRelationship(componentA, componentB,requiredInterface,providedInterface,null,null, RelationType.AGGREGATION);
		assertFalse(result);

		result = umlDiagram.createRelationship(componentA, componentB, null, providedInterface, null, null, RelationType.AGGREGATION);
		assertTrue(result);
		result = umlDiagram.createRelationship(componentA, componentB, null, providedInterface, null, null, RelationType.AGGREGATION);
		assertFalse(result);

		umlDiagram.addPort(portA, "ComponentA");
		result = umlDiagram.validatePort(portA, componentA);
		assertTrue(result);
		umlDiagram.addPort(portB, "ComponentB");
		result = umlDiagram.validatePort(portB, componentB);
		assertTrue(result);

		result = umlDiagram.createRelationship(componentA, componentB,requiredInterface,providedInterface,null,portB, RelationType.AGGREGATION);
		assertTrue(result);   
		result = umlDiagram.createRelationship(componentA, componentB,requiredInterface,providedInterface,null,portB, RelationType.AGGREGATION);
		assertFalse(result);   
		result = umlDiagram.createRelationship(componentA, componentB,requiredInterface,providedInterface,null,portB, RelationType.AGGREGATION);
		assertFalse(result);

		result = umlDiagram.createRelationship(componentA, componentB,null,providedInterface,null,portB, RelationType.AGGREGATION);
		assertTrue(result);   
		result = umlDiagram.createRelationship(componentA, componentB,null,providedInterface,null,portB, RelationType.AGGREGATION);
		assertFalse(result);


		// create relationship with two component, two interface, two port.
		result = umlDiagram.createRelationship(componentA, componentB,requiredInterface,providedInterface,portA,portB, RelationType.AGGREGATION);
		assertTrue(result);   
		result = umlDiagram.createRelationship(componentA, componentB,requiredInterface,providedInterface,portA,portB, RelationType.AGGREGATION);
		assertFalse(result);

		resultList = umlDiagram.getRelationship(componentA);
		assertEquals(resultList.size(),6);
		resultList = umlDiagram.searchRelationship(componentA, componentB);
		assertEquals(resultList.size(),6);
	}

	@Test
	public void deleteComponentTest(){
		boolean result = umlDiagram.deleteComponentByName("ComponentA");
		assertTrue(result);
	}
	
	@Test
	public void deleteInterfaceTest() {
		umlDiagram.addInterface(requiredInterface,"ComponentA");
		boolean result = umlDiagram.deleteInterfaceByName("RequiredInterface", "ComponentA");
		assertTrue(result);
	}

	@Test
	public void deletePortTest() {
		umlDiagram.addPort(portA, "ComponentA");
		boolean result = umlDiagram.deletePortByName("PortA", "ComponentA");
		assertTrue(result);
	}
	
	@Test
	public void deleteRelationshipTest() {
		Component componentC = new Component("ComponentC");
		umlDiagram.addComponent(componentB);
		umlDiagram.addComponent(componentC);
		umlDiagram.createRelationship(componentA, componentB, null, null, null, null, RelationType.AGGREGATION);
		umlDiagram.createRelationship(componentC, componentA, null, null, null, null, RelationType.ASSOCIATION);
		Relationship relationship =  new Relationship(componentA, componentB, null, null, null, null, RelationType.AGGREGATION);
		Relationship relationship2 =  new Relationship(componentC, componentA, null, null, null, null, RelationType.ASSOCIATION);
		
		Iterator<IRelationship> i = umlDiagram.getRelationship(componentA).iterator();
		while(i.hasNext()) {
			Relationship r = (Relationship) i.next();
			boolean result1 = r.equals(relationship);
			boolean result2 = r.equals(relationship2);
			assertTrue(result1||result2);
		}		
		
		assertEquals(umlDiagram.searchRelationship(componentC, componentA).isEmpty(), false);
		assertEquals(umlDiagram.deleteRelationship(relationship2), true);
		assertEquals(umlDiagram.searchRelationship(componentA, componentB).get(0).equals(relationship), true);
		assertEquals(umlDiagram.searchRelationship(componentC, componentA).isEmpty(), true);
	}
	
	@Test
	public void ownerOfInterfaceTest() {
		umlDiagram.addInterface(requiredInterface,"ComponentA");
		assertEquals(umlDiagram.ownerOfInterface(requiredInterface), componentA);
	}
	
	@Test
	public void ownerOfPortTest() {
		umlDiagram.addPort(portA, "ComponentA");
		assertEquals(umlDiagram.ownerOfPort(portA), componentA);
	}
	
	@Test
	public void modifyComponentNameTest() {
		umlDiagram.modifyComponentName("ComponentA", "Component_A");
		assertEquals(componentA.getName(), "Component_A");
	}
	
	@Test
	public void modifyInterfaceNameTest() {
		umlDiagram.addInterface(requiredInterface,"ComponentA");
		umlDiagram.modifyInterfaceName("RequiredInterface", "RequiredInterface2", "ComponentA");
		assertEquals(requiredInterface.getName(), "RequiredInterface2");
	}
	
	@Test
	public void modifyPortNameTest() {
		umlDiagram.addPort(portA, "ComponentA");
		umlDiagram.modifyPortName("PortA", "Port_A", "ComponentA");
		assertEquals(portA.getName(), "Port_A");
	}
	
	@Test
	public void searchInterfaceTest() {
		umlDiagram.addInterface(requiredInterface,"ComponentA");
		ArrayList<ComponInterface> componInterfaceList = umlDiagram.searchInterface("RequiredInterface");
		assertEquals(componInterfaceList.get(0), requiredInterface);
	}
	
	@Test
	public void searchPortTest() {
		umlDiagram.addPort(portA, "ComponentA");
		ArrayList<Port> portList = umlDiagram.searchPort("PortA");
		assertEquals(portList.get(0), portA);
	}
	
	@Test
	public void searchRelationshipTest() {
		umlDiagram.addComponent(componentB);
		umlDiagram.createRelationship(componentA, componentB, null, null, null, null, RelationType.AGGREGATION);
		Relationship relationship =  new Relationship(componentA, componentB, null, null, null, null, RelationType.AGGREGATION);
		ArrayList<IRelationship> relationshipList = umlDiagram.searchRelationship(componentA, componentB);
		assertEquals(relationship.equals((Relationship)(relationshipList.get(0))), true);
	}
	
	@Test
	public void getRelationshipTest() {
		umlDiagram.addComponent(componentB);
		umlDiagram.createRelationship(componentA, componentB, null, null, null, null, RelationType.AGGREGATION);
		Relationship relationship =  new Relationship(componentA, componentB, null, null, null, null, RelationType.AGGREGATION);
		assertEquals(relationship.equals((Relationship) (umlDiagram.getRelationship(componentA).get(0))), true);
	}
	
}